# foundry-macros

This repo contains macros intended to be used with the following modules:

- [DnD5e system](https://foundryvtt.com/packages/dnd5e)
- [Midi Quality of Life Improvements](https://foundryvtt.com/packages/midi-qol)
- [Item Macro](https://foundryvtt.com/packages/itemacro)
- [Automated Animations](https://foundryvtt.com/packages/autoanimations)

Unless otherwise noted in the macro, the standard way of using these macros is:
1. Edit the item (or spell, or feature) you want to apply the macro to
2. Click the `Item Macro` button on the title bar, copy-paste the selected macro there, then save it
3. Still in the edit popup, navigate to the `Details` tab
4. Scroll down to the `On Use Macros` section
5. Add a new macro. Use `ItemMacro` as the name, then look at the top of the macro to get the type.
