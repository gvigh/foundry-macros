// On Use Macros: Before Damange Application
const caster = canvas.tokens.get(args[0].tokenId);
const template = canvas.templates.get(args[0].templateId);

const preExplosionEffect = "jb2a.spell_projectile.ice_shard.blue.60ft";
const explosionEffect = "jb2a.ice_spikes.radial.burst.white"
const postExplosionEffect = "jb2a.impact.ground_crack.frost.01.white"

await Sequencer.Preloader.preloadForClients([preExplosionEffect, explosionEffect, postExplosionEffect])

new Sequence()
    .effect()
        .file(preExplosionEffect)
        .zIndex(1)
        .atLocation(caster)
        .stretchTo(template)
    .wait(1200)
    .effect()
        .file(explosionEffect)
        .zIndex(2)
        .atLocation(template)
        .randomRotation()
        .scale(0.5)
        .scaleIn(0.2, 222)
    .wait(1500)
    .effect()
        .file(postExplosionEffect)
        .zIndex(1)
        .belowTokens()
        .randomRotation()
        .scale(0.5)
        .duration(6000)
        .fadeIn(500)
        .fadeOut(2000)
    .atLocation(template)
    .thenDo(async function(){
        canvas.scene.deleteEmbeddedDocuments("MeasuredTemplate", [args[0].templateId]);
    })
.play();

// Wait until part of the animation is finished before processing the damage
await new Promise(r => setTimeout(r, 2400));