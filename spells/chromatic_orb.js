// On Use Macros: After targeting complete
//  ... and also: After Attack Roll
//  ... and also: Before Damage Roll
//  ... and also: Before Damange Application
// Manualy set the Damage type to None in the Damage Formula section
// Last updated for midi-qol 11.0.20

console.warn("Chromatic orb called with: " + args[0].macroPass)
const currentStage = args[0].macroPass;
const flow = MidiQOL.Workflow.getWorkflow(args[0].uuid);

if (currentStage == "preItemRoll") {
    selectType(flow)
}

if (currentStage == "postAttackRoll") {
    await Sequencer.Preloader.preloadForClients([flow._macro_projectileEffect, flow._macro_explosionEffect])
    buildEffects(args, flow);
    await fireOrb(flow)

}

async function fireOrb(flow) {
    flow.defaultDamageType = flow.overrideDamageType;
    flow._macro_effects.forEach(effect => effect.play());
    await new Promise(r => setTimeout(r, 1000));
}


function selectType(flow) {
    flow.overrideDamageType = "acid"
    flow._macro_projectileEffect = "jb2a.dancing_light.green";
    flow._macro_explosionEffect = "jb2a.toll_the_dead.green.shockwave";
    new Dialog({
        title: "Chromatic Orb",
        content: "<p>Choose damage type for chromatic orb:</p>",
        buttons: {
            acid: {
                icon: '<i class="fas fa-vial"></i>',
                label: "Acid",
                callback: () => {
                    flow.overrideDamageType = 'acid'
                    flow._macro_projectileEffect = "jb2a.dancing_light.green";
                    flow._macro_explosionEffect = "jb2a.toll_the_dead.green.shockwave";
                }

            },
            cold: {
                icon: '<i class="fas fa-icicles"></i>',
                label: "Cold",
                callback: () => {
                    flow.overrideDamageType = 'cold'
                    flow._macro_projectileEffect = "jb2a.dancing_light.blueteal";
                    flow._macro_explosionEffect = "jb2a.impact_themed.ice_shard.blue";
                }
            },
            fire: {
                icon: '<i class="fas fa-fire"></i>',
                label: "Fire",
                callback: () => {
                    flow.overrideDamageType = 'fire'
                    flow._macro_projectileEffect = "jb2a.dancing_light.yellow";
                    flow._macro_explosionEffect = "jb2a.firework.0" +  + (Math.floor(Math.random() * 1) +1) + ".orangeyellow.0" + (Math.floor(Math.random() * 1) +1);
                }
            },
            lightning: {
                icon: '<i class="fas fa-bolt"></i>',
                label: "Lightning",
                callback: () => {
                    flow.overrideDamageType = 'lightning'
                    flow._macro_projectileEffect = "jb2a.lightning_ball.blue";
                    flow._macro_explosionEffect = "jb2a.explosion.02.blue";
                }
            },
            poison: {
                icon: '<i class="fas fa-flask"></i>',
                label: "Poison",
                callback: () => {
                    flow.overrideDamageType = 'poison'
                    flow._macro_projectileEffect = "jb2a.dancing_light.purplegreen";
                    flow._macro_explosionEffect = "jb2a.toll_the_dead.green.skull_smoke";
                }
            },
            thunder: {
                icon: '<i class="fas fa-cloud"></i>',
                label: "Thunder",
                callback: () => {
                    flow.overrideDamageType = 'thunder'
                    flow._macro_projectileEffect = "jb2a.lightning_ball.blue";
                    flow._macro_explosionEffect = "jb2a.explosion.04.blue";
                }
            }
        },
        default: "acid"
    }).render(true);
}

async function buildEffects(args, flow) {
    const casterToken = canvas.scene.tokens.find(token => token.actor.id == args[0].actor._id)
    flow._macro_effects = [];
    for (const target of args[0].targets) {
        // Add spell effect
        const isHit = args[0].hitTargets?.filter(item => item.id == target.id).length > 0;
        const targetPosition = isHit ? target : getRandomPointAround(target.data.x, target.data.y, target.data.width * target.data.scale);
        const spellEffect = new Sequence()
            .effect()
                .file(flow._macro_projectileEffect)
                .scale(0.4)
                .atLocation(casterToken)
                .moveTowards(targetPosition, {ease: "easeInCubic"})
                .moveSpeed(1000)
            .waitUntilFinished()
            .effect()
                .file(flow._macro_explosionEffect)
                .scale(0.7)
                .atLocation(targetPosition)
        flow._macro_effects.push(spellEffect);
    }
    if (flow.isFumble) {
        // If the attack is a fumble, there will be no damage application setp, so play the animation now
        await Sequencer.Preloader.preloadForClients([flow._macro_projectileEffect, flow._macro_explosionEffect])
        flow._macro_effects.forEach(effect => effect.play());
    }
}

function getRandomPointAround(x, y, size) {
    const radius = size * canvas.dimensions.size * 0.60;
    const angle = Math.random()*Math.PI*2;
    return {x: x + Math.cos(angle)*radius, y: y + Math.sin(angle)*radius }
}
