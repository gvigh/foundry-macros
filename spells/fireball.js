// On Use Macros: Before Damange Application
const caster = canvas.tokens.get(args[0].tokenId);
const template = canvas.templates.get(args[0].templateId);

const preExplosionEffect = "jb2a.fireball.beam.orange";
const explosionEffect = "jb2a.fireball.explosion.orange"
const postExplosionEffect = "jb2a.ground_cracks.orange.0" + (Math.floor(Math.random() * 2) +1);

await Sequencer.Preloader.preloadForClients([preExplosionEffect, explosionEffect, postExplosionEffect])

new Sequence()
    .effect()
        .file(preExplosionEffect)
        .zIndex(1)
        .atLocation(caster)
        .stretchTo(template)
    .wait(2200)
    .effect()
        .file(explosionEffect)
        .zIndex(2)
        .atLocation(template)
        .randomRotation()
        .scale(1.3)
        .scaleIn(0.2, 222)
    .wait(1500)
    .effect()
        .file(postExplosionEffect)
        .zIndex(1)
        .belowTokens()
        .randomRotation()
        .scale(1.3)
        .duration(6000)
        .fadeIn(500)
        .fadeOut(2000)
    .atLocation(template)
    .thenDo(async function(){
        canvas.scene.deleteEmbeddedDocuments("MeasuredTemplate", [args[0].templateId]);
    })
.play();

// Wait until part of the animation is finished before processing the damage
await new Promise(r => setTimeout(r, 2400));
