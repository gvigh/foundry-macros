// On Use Macros: Before Damange Application

const initialEffect = "jb2a.divine_smite.target.blueyellow";
const explosionEffect = "jb2a.flames.01.orange";

await Sequencer.Preloader.preloadForClients([initialEffect, explosionEffect]);

const effects = [];

for (const target of args[0].targets) {
    const saveFailed = args[0].failedSaves?.filter(item => item.id == target.id).length > 0;
    const targetPosition = target;
    const spellEffect = new Sequence()
        .effect()
            .file(initialEffect)
            .atLocation(targetPosition)
            .wait(1000)
    if (saveFailed) {
        spellEffect.effect()
            .file(explosionEffect)
            .randomRotation()
            .atLocation(targetPosition)
            .fadeOut(1500)
    }

    effects.push(spellEffect);
}

effects.forEach(effect => effect.play());
await new Promise(r => setTimeout(r, 1000));