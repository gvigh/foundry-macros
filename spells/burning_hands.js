// On Use Macros: Before Damange Application
const caster = canvas.tokens.get(args[0].tokenId);
const template = canvas.templates.get(args[0].templateId);

const effect = "jb2a.burning_hands.01.orange";

await Sequencer.Preloader.preloadForClients([effect])

new Sequence()
    .effect()
        .file(effect)
        .zIndex(1)
        .atLocation(caster)
        .stretchTo(template)
        .thenDo(async function(){
            canvas.scene.deleteEmbeddedDocuments("MeasuredTemplate", [template]);
        })
.play();

// Wait until part of the animation is finished before processing the damage
await new Promise(r => setTimeout(r, 200));
